<?php
namespace crawler;

use crawler\exceptions\DBALException;
use crawler\factories\Factory;
use crawler\db\MySql;

class Proxy
{
    /**
     * @var \crawler\db\MySql $db
     */
    private $db;

    private $tableName;
    /**
     * ID прокси, который сейчас используется
     * @var integer
     */
    private $id;
    /**
     * IP прокси, который сейчас используется
     * @var integer
     */
    private $address;
    /**
     * Порт прокси, который сейчас используется
     * @var integer
     */
    private $port;
    /**
     * Тип прокси, который сейчас используется
     * @var integer
     */
    private $type;

    private $renewed = false;

    public function __construct(MySql $db)
    {
        $this->db = $db;
    }

    // INITIAL ACTIONS

    /**
     * Накатываем прокси из файла
     * 
     * @param string $file
     * @return void
     */
    public function renewList($file)
    {
        if (!file_exists($file)) {
            return $this;
        }
        echo "Load proxies $file\n";

        if (!$this->renewed) {
            $this->deleteOld();
        }
        try {
            $this->saveList(file($file));
        } catch (DBALException $e) {
            echo 'Error occured during saving proxies list.';
        }
        unlink($file);
        $this->renewed = true;

        echo "Proxies from $file loaded\n";

        return $this;
    }

    private function saveList(array $proxyList)
    {
        $data = array_map(function($item) {
            $arr = explode(' ', $item);
            list($type, $address, $port) = explode(':', $arr[0]);
            $port = filter_var($port, FILTER_SANITIZE_NUMBER_INT) + (count($arr) > 1 ? 20000 : 0);
            return [
                'ADDRESS_PROXY' => $address,
                'PORT_PROXY' => $port,
                'TYPE' => $type,
                'IS_USED' => 0,
                'IS_LIVE' => 1
            ];
        }, $proxyList);

        $this->db->insertMultiple($this->tableName, $data);
    }

    private function deleteOld()
    {
        $this->db->query("DELETE FROM {$this->tableName}");
        $this->db->query("ALTER TABLE {$this->tableName} AUTO_INCREMENT = 1");
    }

    /**
     * @return Proxy
     */
    public function setAsDead()
    {
        $this->_update(['IS_LIVE' => 0]);
        Logger::err("Proxy {$this->address}:{$this->port} is dead.", 'proxy');
        return $this;
    }

    private function changeAvailability(int $value)
    {
        if (!is_null($this->id)) {
            $this->_update(['IS_USED' => $value]);
        }
    }

    public function lock()
    {
        $this->changeAvailability(1);
    }

    /**
     * @return Proxy
     */
    public function deleteDead()
    {
        $this->_delete("`IS_LIVE` = 0");
        return $this;
    }

    /**
     * @param boolean $rand
     * @return array
     */
    public function getFree($rand=true)
    {
        return $this->db->query("
            SELECT *
            FROM {$this->tableName}
            WHERE IS_USED = 0 AND IS_LIVE = 1
            " . ($rand ? 'ORDER BY RAND()' : '') . "
            LIMIT 1
        ")->fetch();
    }

    public function free()
    {
        $this->changeAvailability(0);
    }

    /**
     * @return Proxy
     */
    public function freeAll()
    {
        $this->db->update($this->tableName, [
            'query' => 'IS_USED = 0'
        ]);
        return $this;
    }

    public function change($rand=true)
    {
        if ($proxy = $this->getFree($rand)) {
            $this
                ->setId($proxy['ID'])
                ->setAddress($proxy['ADDRESS_PROXY'])
                ->setPort($proxy['PORT_PROXY'])
                ->setType($proxy['TYPE'])
                ->lock();

            return true;
        }
        Logger::err('No free proxy', 'proxy');
        return false;
    }

    /**
     * Отмечаем время получения 
     * 
     * @param integer $id
     * @return Proxy
     */
    public function setFirstUseTime()
    {
        $this->_update(['FIRST_USED' => date('Y-m-d H:i:s')]);
        $this->setDirty();
        return $this;
    }

    /**
     * Отмечаем время получения 
     * 
     * @param integer $id
     * @return Proxy
     */
    public function setLastUseTime()
    {
        $this->_update(['LAST_USED' => date('Y-m-d H:i:s')]);
        return $this;
    }

    /**
     * Увеличиваем счетчик удачных запросов
     * 
     * @return Proxy
     */
    public function encreaseRequestsCounter()
    {
        $requests = (int)$this->_select('REQUESTS_COUNT');
        $this->_update(['REQUESTS_COUNT' => ++$requests]);
        /*$this->db->update(
            'requests',
            [
                'proxy_ip' => $this->proxy->getAddress(),
                'start_time' => microtime(true),
            ],
            ['ignore' => true]
        );*/
        return $this;
    }

    /**
     * @return Proxy
     */
    public function setClean()
    {
        $this->_update(['IS_CLEAN' => 1]);
        return $this;
    }

    /**
     * @return Proxy
     */
    public function setDirty()
    {
        $this->_update(['IS_CLEAN' => 0]);
        return $this;
    }

    /**
     * @return boolean
     */
    public function isClean()
    {
        return $this->_select('IS_CLEAN');
    }

    /**
     * @return Proxy
     */
    public function encreaseErrCounter()
    {
        $errors = $this->getErrCounter();
        $this->setErrCounter($errors + 1);
        return $this;
    }

    /**
     * @param integer $errCounter
     * @return Proxy
     */
    public function setErrCounter($errCounter)
    {
        $this->_update(['ERRORS' => $errCounter]);
        return $this;
    }

    /**
     * @return integer
     */
    public function getErrCounter()
    {
        return (int)$this->_select('ERRORS');
    }

    /**
     * Количество ошибок превышает допустимую норму
     * @return boolean
     */
    public function errorsOverflow()
    {
        return $this->getErrCounter() >= 10;
    }

    /**
     * @param mixed $fields
     * @param string $where
     * @return array
     */
    private function _select($field, $where=null)
    {
        if (is_null($where)) {
            $where = "ID = {$this->id}";
        }
        if ($result = $this->db->select(
            $this->tableName,
            $field,
            $where
        )) {
            return $result[$field];
        }
    }

    /**
     * @param array data
     * @return Proxy
     */
    private function _update($data)
    {
        $this->db->update(
            $this->tableName,
            [
                'data' => $data,
                'where' => 'ID = :ID',
                'bind' => [
                    'ID' => $this->id
                ]
            ]
        );
        return $this;
    }

    /**
     * @param string $where
     * @return Proxy
     */
    private function _delete($where=null)
    {
        if (is_null($where)) {
            $where = "ID = {$this->id}";
        }
        $this->db->query("
            DELETE FROM `{$this->tableName}`
            WHERE $where
        ");
        return $this;
    }

    /**
     * @return void
     */
    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @param integer $id
     * @return Proxy
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $address
     * @return Proxy
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $port
     * @return Proxy
     */
    public function setPort($port)
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @return string
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param string $type
     * @return Proxy
     */
    public function setType($type)
    {
        switch ($type) {
            case 'socks4':
            case 'CURLPROXY_SOCKS4':
                $this->type = 'CURLPROXY_SOCKS4';
                break;
            case 'http':
            case 'CURLPROXY_HTTP':
                $this->type = 'CURLPROXY_HTTP';
                break;
            case 'socks':
            case 'CURLPROXY_SOCKS5':
                $this->type = 'CURLPROXY_SOCKS5';
                break;
            default:
                $this->type = 'CURLPROXY_SOCKS5';
                break;
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
