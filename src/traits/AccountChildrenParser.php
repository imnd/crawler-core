<?php
namespace crawler\traits;

use crawler\helpers\ArrayHelper;

trait AccountChildrenParser
{
    /**
     * Текущая таблица акков, из которой извлекаем посты из Инстаграма
     * @var string $currentAccountLogin
     */
    protected $currentAccountTable;
    /**
     * Текущий индекс id акка в массиве $accParseIds акков
     * @var array $lastAccParseId
     */
    protected $curAccParseIdIndex = 0;
    /**
     * Массив id по которым будут извлекаться акки
     * @var array $accParseIds
     */
    protected $accParseIds;

    protected function isStoreTableFits()
    {
        // Устанавливаем текущую таблицу аккаунтов, чтобы брать оттуда ID
        if (!$this->setCurAccTable()) {
            return false;
        }
        // следующий ID акка для парсинга
        return $this->setNextParseId();
    }

    /**
     * Устанавливаем текущую таблицу аккаунтов, чтобы брать оттуда ID
     * Обнуляем $accParseIds
     * @return boolean
     */
    protected function setCurAccTable()
    {
        $accTable = $this->accDomainModel->getPrefix() . $this->getTableNum($this->storeTableName);
        //Logger::log("Try account table $accTable.");
        $freeTables = $this->accDomainModel->getFreeTables(false);
        $freeTables = ArrayHelper::flatten($freeTables, 'table_name');
        //Logger::log("Free tables are " . print_r($freeTables, true));
        if (!in_array($accTable, $freeTables)) {
            return false;
        }
        if ($this->currentAccountTable != $accTable) {
            $this->accParseIds = null;
        }
        $this->currentAccountTable = $accTable;
        //Logger::log("Current account table is {$this->currentAccountTable}.");
        return true;
    }

    /**
     * @inheritdoc
     */
    protected function setNextParseId()
    {
        //Logger::log("curAccParseIdIndex = {$this->curAccParseIdIndex}");
        if (is_null($this->accParseIds)) {
            //Logger::log("accParseIds is null");
            if (is_null($this->currentAccountId)) {
                // берем последнюю запись из таблицы постов
                $this->currentAccountId = $this->domainModel->getLastId($this->storeTableName);
            }
            $this->accParseIds = $this->accDomainModel->getParseIds($this->currentAccountTable, $this->currentAccountId);
            //Logger::log("accParseIds set to " . print_r($this->accParseIds, true));
        } else {
            //Logger::log("accParseIds are " . print_r($this->accParseIds, true));
        }
        if (isset($this->accParseIds[$this->curAccParseIdIndex])) {
            // берем следующую после последней спарсеной запись из таблицы акков
            $this->currentAccountId = $this->accParseIds[$this->curAccParseIdIndex];
            Logger::log("Set next account id = {$this->currentAccountId} in table {$this->storeTableName}");
            $this->curAccParseIdIndex++;
            return true;
        }
        // таблица акков пустая. сканировать нечего.
        $this->curAccParseIdIndex = 0;
        return false;
    }

    protected function setNewProxy($rand=false)
    {
        parent::setNewProxy($rand);

        \InstagramScraper\Instagram::setProxy([
            'address' => $this->proxy->getAddress(),
            'port' => $this->proxy->getPort(),
            'type' => $this->proxy->getType(),
            'timeout' => 15
        ]);
    }
}
