<?php
namespace crawler\models;

use crawler\helpers\ArrayHelper;
use crawler\exceptions\ModelException;

class AccountModel extends Model
{
    /** @const строк в таблице */
    const TABLE_SIZE = 100000;

    /**
     * @inheritdoc
     */
    protected $prefix = 'acc';
    /**
     * @inheritdoc
     */
    protected $dischargeField = 'is_full';

    /**
     * @inheritdoc
     */
    public function createTables(int $from, int $to)
    {
        $this->checkRange($from, $to);
        $this->createMasterTable();

        for ($i = $from; $i <= $to; $i++) {
            $tableName = $this->getTableNameByNum($i);
            if (!$this->db->isTableExists($tableName)) {
                $this->db->query("
                    CREATE TABLE `$tableName` (
                        `account_id` int(11) AUTO_INCREMENT UNSIGNED NOT NULL,
                        `username` varchar(100) NULL,
                        `full_name` varchar(100) NULL,
                        `biography` text NULL,
                        `external_link` varchar(255) NULL,
                        `avatar_url` varchar(255) NULL,
                        `media_count` int(11) UNSIGNED NULL,
                        `followers_count` int(11) UNSIGNED NULL,
                        `follows_count` int(11) UNSIGNED NULL,
                        `is_private` int(11) UNSIGNED NULL,
                        `is_verified` int(11) UNSIGNED NULL,
                        `main_time_posts` SMALLINT(5) unsigned NOT NULL DEFAULT '0',
                        `last_post_date` DATETIME DEFAULT NULL,
                        `status` varchar(100) NULL DEFAULT " . self::STATUS_READY . ",
                        `created_at` DATETIME NULL COMMENT 'Время создания аккаунта в инсте',
                        `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        KEY `username` (`username`)
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
                ");
                $autoIncrement = ($i - 1) * self::TABLE_SIZE;
                $this->db->query("
                    ALTER TABLE $tableName
                    AUTO_INCREMENT = $autoIncrement
                ");
                $this->db->insert(
                    $tableName,
                    ['username' => 'dummy'],
                    ['ignore' => true]
                );
            }
            $this->db->insert($this->getMasterTableName(), ['table_name' => $tableName], ['ignore' => true]);
        }
    }

    /**
     * Массив ID для парсинга
     * 
     * @return array
     */
    public function getParseIds($tableName, $firstId = null)
    {
        if (is_null($firstId)) {
            // берем первую запись из таблицы
            $firstId = $this->getFirstId($tableName);
        }
        if (null === $maxParseId = $this->getLastId($tableName)) {
            throw new ModelException("Table $tableName is empty");
        }
        // берем диапазон id из таблицы акков со следующей
        // после $firstId записи до $maxParseId
        if ($rows = $this->db->query("
            SELECT account_id
            FROM $tableName
            WHERE
                    account_id > $firstId
                AND account_id <= $maxParseId
                AND status = " . self::STATUS_READY
            )->fetchAll()) {
            return ArrayHelper::flatten($rows, 'account_id');
        }
    }
}
