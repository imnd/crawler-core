<?php
namespace crawler\models;

use crawler\db\MySql;

class Model
{
    /** @const Значения поля $dischargeField */
    const DISCHARGED = 1;
    const NOT_DISCHARGED = 0;
    const MASTER_TABLE_STATUS_FREE = "'free'";
    const STATUS_READY = "'ready'";

    /**
     * @var crawler\db\MySql
     */
    protected $db;
    protected $prefix;
    protected $suffix = '00';
    protected $masterSuffix = '_master';
    /**
     * Флаг, что с таблицей больше не работаем
     * @var boolean string integer array mixed 
     */
    protected $dischargeField;
    /**
     * Имя таблицы модели, в которую складываются извлеченные данные
     * @var string $tableName
     */
    protected $tableName;

    public function __construct(MySql $db)
    {
        $this->db = $db;
    }

    /**
     * @param integer $from
     * @param integer $to
     * @return void
     */
    protected function checkRange(int $from, int $to)
    {
        if ($from < 1) {
            throw new Exception('first var can not be smaller then 1');
        }
        if ($from >= $to) {
            throw new Exception('first var can not be bigger or equal then second var');
        }
    }

    # ЗАПРОСЫ

    public function createTables(int $from, int $to)
    {
        $this->checkRange($from, $to);
        $this->createMasterTable();

        for ($i = $from; $i <= $to; $i++) {
            $tableName = $this->getTableNameByNum($i);
            if (!$this->db->isTableExists($tableName)) {
                $this->db->query("
                    CREATE TABLE `$tableName` ({$this->getTableSchema($i)}) ENGINE=MyISAM DEFAULT CHARSET=utf8;
                ");
            }
            $this->db->insert($this->getMasterTableName(), ['table_name' => $tableName], ['ignore' => true]);
        }
    }

    public function createMasterTable()
    {
        if (!$this->db->isTableExists($this->getMasterTableName())) {
            $this->db->query("
                CREATE TABLE `{$this->getMasterTableName()}` (
                    `table_name` varchar(100) NOT NULL,
                    `status` varchar(100) NOT NULL DEFAULT " . self::MASTER_TABLE_STATUS_FREE . ",
                    `{$this->dischargeField}` tinyint(1) NOT NULL DEFAULT " . self::NOT_DISCHARGED . ",
                    PRIMARY KEY (`table_name`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            ");
        }
    }

    public function getSlaveTables(string $status, $dischargeVal = null)
    {
        $where = "status = '$status'";
        if (!is_null($dischargeVal))
            $where .= " AND `{$this->dischargeField}` = $dischargeVal";

        return $this->db->query("
            SELECT *
            FROM {$this->getMasterTableName()}
            WHERE $where
        ")->fetchAll();
    }

    /**
     * Удалить таблицы
     * @param integer $from
     * @param integer $to
     * @return void
     */
    public function dropTables(int $from, int $to)
    {
        $this->checkRange($from, $to);

        for ($i = $from; $i <= $to; $i++) {
            $tableName = "{$this->prefix}$i{$this->suffix}";
            $this->db->drop($tableName);
        }

        $this->db->drop($this->getMasterTableName());
    }

    public function unlockAllTablesForParse()
    {
        $this->db->query("
            UPDATE {$this->getMasterTableName()}
            SET status = " . self::MASTER_TABLE_STATUS_FREE . ",
            `{$this->dischargeField}` = " . self::NOT_DISCHARGED
        );
    }

    /**
     * @param string $tableName
     * @param string $status
     * @return void
     */
    public function setStoreTableStatus($tableName, $status)
    {
        $this->db->query("
            UPDATE {$this->getMasterTableName()}
            SET `status` = '$status'
            WHERE table_name = '$tableName'
        ");
    }

    /**
     * @param integer $status
     * @return boolean
     */
    public function setStatus($tableName, $id, $status)
    {
        // todo проверять на наличие записи
        return $this->db->insert($tableName, [
            'account_id' => $id,
            'status' => $status
        ], ['ignore' => true]);
    }

    public function dischargeTable($tableName)
    {
        $this->db->query("
            UPDATE {$this->getMasterTableName()}
            SET `{$this->dischargeField}` = " . self::DISCHARGED . "
            WHERE table_name = '$tableName'
        ");
    }

    /**
     * Извлекает последнюю запись из таблицы $tableName
     * 
     * @param string $tableName
     * @return integer
     */
    public function getLastId($tableName)
    {
        return $this->db->query("
            SELECT MAX(account_id)
            FROM $tableName
        ")->fetchColumn();
    }

    public function getFirstId($tableName)
    {
        return $this->db->query("
            SELECT MIN(account_id)
            FROM $tableName
        ")->fetchColumn();
    }

    public function getNextId($tableName, $id)
    {
        return $this->db->query("
            SELECT MIN(account_id)
            FROM $tableName
            WHERE account_id > $id
        ")->fetchColumn();
    }

    public function getFreeTables($discharged = true)
    {
        $where = "`status` = " . self::MASTER_TABLE_STATUS_FREE;
        if ($discharged) {
            $where .= " AND `{$this->dischargeField}` = " . self::NOT_DISCHARGED;
        }
        return $this->db->query("
            SELECT table_name
            FROM {$this->getMasterTableName()}
            WHERE $where
        ")->fetchAll();
    }

    public function getFreeTable()
    {
        return $this->db->query("
            SELECT table_name
            FROM {$this->getMasterTableName()}
            WHERE `status` = " . self::MASTER_TABLE_STATUS_FREE . " AND `{$this->dischargeField}` = " . self::NOT_DISCHARGED . "
            LIMIT 1
        ")->fetchColumn();
    }

    /**
     * Сохраняем данные модели в БД
     * 
     * @param array $data
     * @return boolean
     */
    public function saveModel(array $data, string $tableName = null)
    {
        if (is_null($tableName)) {
            $tableName = $this->tableName;
        }
        return $this->db->insertMultiple($tableName, array($data));
    }

    /**
     * Сохраняем данные моделей в БД
     * 
     * @param array [$models]
     * @return boolean
     */
    public function saveModels(array $models, string $tableName = null)
    {
        if (is_null($tableName)) {
            $tableName = $this->tableName;
        }
        return $this->db->insertMultiple($tableName, $models);
    }

    public function closeConnection()
    {
        $this->db->closeConnection();
    }

    # Геттеры / Сеттеры

    public function getPrefix()
    {
        return $this->prefix;
    }

    public function getSuffix()
    {
        return $this->suffix;
    }

    public function getMasterTableName()
    {
        return "{$this->prefix}{$this->masterSuffix}";
    }

    /**
     * @param string $tableName
     * @return void
     */
    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @return string
     */
    public function getTableNameByNum($i)
    {
        return "{$this->prefix}$i{$this->suffix}";
    }
}
