<?php
namespace crawler\models;

class UserStoryModel extends Model
{
    /**
     * @inheritdoc
     */
    protected $prefix = 'user_post';
    /**
     * @inheritdoc
     */
    protected $dischargeField = 'is_parsed';
    /** @var Поля таблицы */
    protected $tableFields = [
        'post_id',
        'account_id',
        'short_code',
        'created_at',
        'caption',
        'comments_count',
        'likes_count',
        'link',
        'hr_image',
        'media_type',
        'tags',
    ];

    /**
     * @param integer $i
     * @return string
     */
    protected function getTableSchema($i)
    {
        return "
            `post_id` bigint(20) NOT NULL PRIMARY KEY,
            `account_id` int(11) NOT NULL,
            `caption` varchar(100) NULL,
            `location` varchar(100) NULL,
            `short_code` varchar(100) NULL,
            `likes_count` SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
            `comments_count` SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
            `link` TEXT NULL,
            `hr_image` TEXT NULL,
            `media_type` VARCHAR(100) NULL,
            `tags` TEXT NULL,
            `er` tinyint(2) UNSIGNED NOT NULL,
            `created_at` TIMESTAMP NULL COMMENT 'Время создания поста в инсте',
            `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
        ";
    }
}
