<?php
namespace crawler\models;

class FollowerModel extends Model
{
    /** @const Сколько фоловеров выдергиваем */
    const MEDIAS_COUNT = 10000;

    /**
     * @inheritdoc
     */
    protected $prefix = 'follower';
    /**
     * @inheritdoc
     */
    protected $dischargeField = 'is_parsed';

    /**
     * @param integer $i
     * @return string
     */
    protected function getTableSchema($i)
    {
        return "
            `id` bigint(20) NOT NULL PRIMARY KEY,
            `account_id` int(11) NOT NULL,
            `username` varchar(100) NULL,
            `caption` varchar(100) NULL,
            `posts_count` smallint(11) UNSIGNED NULL,
            `followers_count` smallint(11) UNSIGNED NULL,
            `avatar_url` varchar(255) NULL,
            `selection_id` int(11) UNSIGNED NULL
        ";
    }
}
