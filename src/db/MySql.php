<?php
namespace crawler\db;

use crawler\exceptions\DBALException;

class MySql
{
    /**
     * PHP data object
     * @var \PDO $pdo
     */
    private $pdo;

    /**
     * Creating \PDO object
     * CONNECTION
     * @return void
     */
    public function __construct(array $conf = array())
    {
        $this->pdo = new \PDO(
            "{$conf['type']}:dbname={$conf['name']};host={$conf['addr']};charset={$conf['charset']}",
            $conf['user'],
            $conf['pass'],
            $conf['options']
        );
        $this->query('SET GLOBAL connect_timeout=28800');
        $this->query('SET GLOBAL wait_timeout=28800');
        $this->query('SET GLOBAL interactive_timeout=28800');
    }

    /**
     * query (low_level_handler)
     * $db->query('SELECT ')->fetchAll();
     * 
     * @param string $sql
     * @param array $args
     * @return \PDO::query
     */
    public function query(string $sql, array $args = null)
    {
        if (is_null($args)) {
            return $this->pdo->query($sql);
        }
        // Security deals abit
        foreach ($args as $k => $v) {
            if (is_string($k)) {
                if (preg_match('/[^A-Za-z0-9\_\$]/', $k)) {
                    throw new DBALException("Args key contain banned symbols: $k");
                }
            }
        }
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($args);
        return $stmt;
    }

    /**
     * 
     * 
     * @param boolean string integer array mixed 
     * @return 
     */
    public function select(string $table, $fields, $where='')
    {
        if (is_array($fields)) {
            $fields = implode(',', $fields);
        }
        $query = "
            SELECT $fields
            FROM $table
        ";
        if (!empty($where)) {
            $query .= "
                WHERE $where
            ";
        }
        return $this->query($query)->fetch();
    }

    /**
     * INSERT
     * $insertID = $db->insert(
     *      'orders',
     *      [
     *          'ordStatus' => 'Сякой',
     *          'ordRecallTime' => '2016-12-05',
     *          'ordMeasureTime' => '2016-12-16'
     *      ],
     *      [
     *          'ignore' => false // default false | turn on 'insert IGNORE into'
     *      ]
     *  );
     *  return true o success or false on fail
     * @return boolean
     */
    public function insert(string $table, array $data, array $param = []): bool
    {
        // param validation
        if (preg_match('/[^A-Za-z0-9\_\$]/', $table)) {
            throw new DBALException("Table name contain banned symbols: $table");
        }
        if (empty($data)) {
            throw new DBALException('"data" array can not be empty');
        }

        // Working with array and prepared statement
        foreach (array_keys($data) as $field) {
            if (preg_match('/[^A-Za-z0-9\_\$]/', $field)) {
                throw new DBALException("Column name contain banned symbols: $field");
            }

            $fields[] = "`$field`";
            $values[] = ":$field";
        }

        $fields = implode(',', $fields);
        $values = implode(',', $values);

        $ignore = !empty($param['ignore']) ? 'IGNORE' : '';

        if (!$query = $this->prepare("INSERT $ignore INTO `$table` ($fields) VALUE ($values)")) {
            return false;
        }

        return $query->execute($data);
    }

    /**
     * Вставка нескольких строк за раз
     * @param string $tableName
     * @param array $fields
     * @param array $data
     * @param boolean $ignore
     * @return boolean
     */
    public function insertMultiple($tableName, array $data, $ignore = true): bool
    {
        if (empty($data)) {
            return false;
        }

        $dataFields = array_map(function($field) {
            return "`$field`";
        }, array_keys($data[0]));

        $questionMarks = array_map(function($item) {
            return '('  . substr(str_repeat('?,', sizeof($item)), 0, -1) . ')';
        }, $data);

        $insertValues = [];
        foreach ($data as $item) {
            $insertValues = array_merge($insertValues, array_values($item));
        }
        $this->pdo->beginTransaction();
        $stmt = $this->pdo->prepare("INSERT INTO $tableName (" . implode(',', $dataFields ) . ') VALUES ' . implode(',', $questionMarks));
        try {
            $stmt->execute($insertValues);
        } catch (PDOException $e) {
            Logger::err("Error #{$e->getCode()} on insert models, message: {$e->getMessage()}");
        }
        return $this->pdo->commit();
    }

    /**
     * UPDATE
     * 
     * !important. Return true on successful command execution,
     * even if nothing were updated or updated row were not exists
     * !important. Return true on success command execution not especially on update
     * 
     * ----- USAGE -----
     *
     * 1) Super simple
     *      $db->update(
     *          'tableName',
     *          [
     *              'query' => 'country = "Russia"'
     *          ]
     *      );
     *
     * 2) Simple and safety
     *      $db->update(
     *          'tableName',
     *          [
     *              'query' => 'country = :country WHERE capital = :capital',
     *              'bind' => [
     *                  'country' => $yourCountry,
     *                  'capital' => $yourCapital
     *              ]
     *          ]
     *      );
     *
     * 3) Simple and safety if you have a big count of values to update
     *      $db->update(
     *          'tableName',
     *          [
     *              'data' => [
     *                  'house' => $house,
     *                  'address' => $address,
     *                  'floorArea' => $floorArea,
     *                  'wallArea' => $wallArea
     *              ],
     *              'where' => 'order_id = :order_id AND name = :name',
     *              'bind' => [
     *                  'order_id' => $order_id,
     *                  'name' => $name
     *              ]
     *          ]
     *      );
     * 
     * @param string $table
     * @param array $param
     * @return void
     */
    public function update(string $table, array $param)
    {
        // Checking table name
        if (preg_match('/[^A-Za-z0-9\_\$]/', $table)) {
            throw new DBALException("Table name contain banned symbols: $table");
        }
        // Checking an "update way" (through 'query' or 'data')
        if (isset($param['query'])) {
            if (isset($param['data'])) {
                throw new DBALException('You can not use $param[\'query\'] and $param[\'data\'] both at one time');
            }
            // if "update way" - query or data
            // preparing query and check on debug_mode
            if (!$query = $this->prepare("UPDATE $table SET {$param['query']}")) {
                return false;
            }
        } elseif (isset($param['data'])) {
            // 'where' param validation
            $where = isset($param['where']) ? 'WHERE ' . $param['where'] : '';
            // preparing string for prepare()
            foreach ($param['data'] as $k => $v) {
                if (preg_match('/[^A-Za-z0-9\_\$]/', $k)) {
                    throw new DBALException("data key contain banned symbols: $k");
                }

                $fieldKeys[] = "`$k` = :$k";
            }
            $fieldKeys = implode(",", $fieldKeys);
            // preparing query and check on debug_mode
            $query = $this->prepare("UPDATE `$table` SET $fieldKeys $where");
            if (!$query) {return false;}
            // binding elements from data
            foreach ($param['data'] as $k => &$v) {
                $query->bindParam(":$k", $v);
            }
        } else {
            // if update way not declared
            throw new DBALException('You must declare "query" or "data" key inside $param argument');
        }
        // 'bind' param validation and processing
        if (isset($param['bind'])) {
            if (!is_array($param['bind'])) {
                throw new DBALException('Invalid "bind" data type. Correct type is array');
            } else {
                if (empty($param['bind'])) {
                    throw new DBALException('"bind" param can not be empty');
                } else {
                    foreach ($param['bind'] as $k => &$v) { // bindParam working by reference!
                        if (preg_match('/[^A-Za-z0-9\_\$]/', $k)) {
                            throw new DBALException("Bind param key contain banned symbols: $k");
                        }

                        $query->bindParam($k, $v);
                    }
                }
            }
        }
        // executing query
        return $query->execute();
    }

    /**
     * @param string $tableName
     * @return boolean
     */
    public function drop($tableName)
    {
        if ($this->isTableExists($tableName)) {
            return $this->query("DROP TABLE `$tableName`");
        }
    }

    // OTHER

    /**
     * Return - false if debug mode is on
     * Else   - PDOStatement Object with prepared string
     * @param string $query
     * @return 
     */
    public function prepare(string $query)
    {
        return $this->pdo->prepare($query);
    }

    public function info()
    {
        $output = [
            'server' => 'SERVER_INFO',
            'driver' => 'DRIVER_NAME',
            'client' => 'CLIENT_VERSION',
            'version' => 'SERVER_VERSION',
            'connection' => 'CONNECTION_STATUS'
        ];

        foreach ($output as $k => $v) {
            $output[$k] = $this->pdo->getAttribute(constant('\PDO::ATTR_' . $v));
        }

        return $output;
    }

    public function isTableExists(string $tableName): bool
    {
        return !($this->query("SHOW TABLES LIKE '$tableName'")->fetch() === false);
    }

    public function closeConnection()
    {
        $this->pdo = null;
    }
}
