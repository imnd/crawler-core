<?php
namespace crawler\db;

use crawler\exceptions\DBALException;

class Mongo
{
    /**
     * MongoDB client
     */
    private $client;
        
    /**
     * Connecting to MongoDB server made through \Sokil\Mongo\Client class
     * @return void
     */
    public function __construct(array $conf = array())
    {
        $this->client = new \MongoDB\Driver\Manager("mongodb://{$conf['addr']}");
    }

    /**
     * @return 
     */
    public function query($nodeName)
    {
        $collection = $this->client->$nodeName;
        return $collection;
    }

    /**
     * @return boolean
     */
    public function insert(array $data, array $params = []): bool
    {}

    /**
     * Вставка нескольких документов за раз
     * @param array $data
     * @return boolean
     */
    public function insertMultiple(array $data): bool
    {}

    /**
     * @param array $params
     * @return void
     */
    public function update(string $nodeName, array $params)
    {}

    /**
     * @param string $nodeName
     * @return boolean
     */
    public function delete(string $nodeName)
    {}
}
