<?php
namespace crawler;

use crawler\Config;

class Logger
{
    private static $instance;
    private $config;

    public function __construct()
    {}

    /**
     * Singleton
     * 
     * @return Logger
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self;
            self::$instance->setConfig();
        }
        return self::$instance;
    }

    public function setConfig()
    {
        $this->config = new Config;
    }

    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param string $msg
     * @return void
     */
    public static function err($msg, $fileName = 'error')
    {
        self::_log($msg, $fileName);
    }

    /**
     * Запись в файл лога
     * 
     * @param string $msg
     * @return void
     */
    public static function log($msg)
    {
        if (self::getInstance()->getConfig()->getEnv()!=='prod') {
            self::_log($msg, 'log');
        }
    }

    /**
     * Запись в файл лога
     * 
     * @param string $msg
     * @param string $fileName
     * @return void
     */
    private static function _log($msg = '', $fileName = 'log')
    {
        file_put_contents(self::getInstance()->getConfig()->getLogsDir() . "/$fileName.log", date('Y-m-d H:i:s') . ': ' . print_r($msg, true) . PHP_EOL, FILE_APPEND);
    }
}
