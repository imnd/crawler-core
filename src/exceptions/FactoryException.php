<?php
namespace crawler\exceptions;

/**
 * @author Андрей Сердюк
 */
class FactoryException extends \Exception
{
}
