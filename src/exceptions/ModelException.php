<?php
namespace crawler\exceptions;

/**
 * @author Андрей Сердюк
 */
class ModelException extends \Exception
{
}
