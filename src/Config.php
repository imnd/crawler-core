<?php
namespace crawler;

// TODO: сделать Singleton`ом
class Config
{
    private $env = 'prod';
    private $tempDir;
    private $logsDir;
    private $appConf;

    /**
     * @return void
     */
    public function __construct($rootDir = null)
    {
        if (is_null($rootDir)) {
            $rootDir = __DIR__ . '/../../../';
        }
        $this->setEnv(file("$rootDir/env")[0]);
        $this->setAppConf(require "$rootDir/config/crawler-{$this->env}.php");
        $this->setTempDir("$rootDir/temp");
        $this->setLogsDir("{$this->tempDir}/logs");

        ini_set('error_log', "{$this->logsDir}/error.log");
    }

    /**
     * @return void
     */
    public function setTempDir($dir)
    {
        $this->tempDir = $dir;
    }

    /**
     * @return string
     */
    public function getTempDir()
    {
        return $this->tempDir;
    }

    /**
     * @return void
     */
    public function setLogsDir($dir)
    {
        $this->logsDir = $dir;
    }

    /**
     * @return string
     */
    public function getLogsDir()
    {
        return $this->logsDir;
    }

    /**
     * @return void
     */
    public function setEnv($env)
    {
        $this->env = $env;
    }

    /**
     * @return string
     */
    public function getEnv()
    {
        return $this->env;
    }

    /**
     * @return void
     */
    public function setAppConf($appConf)
    {
        $this->appConf = $appConf;
    }

    /**
     * @return string
     */
    public function getAppConf($keys)
    {
        if (!is_array($keys)) {
            $keys = array($keys);
        }
        $ret = $this->appConf;
        foreach ($keys as $key) {
            if (isset($ret[$key])) {
                $ret = $ret[$key];
            } else {
                throw new \ErrorException("config key $key not defined.");
            }
        }
        return $ret;
    }
}
