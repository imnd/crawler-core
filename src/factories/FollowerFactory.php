<?php
namespace crawler\factories;

class FollowerFactory extends Factory
{
    /**
     * Сущность, с которой работает парсер
     * 
     * @return string
     */
    public function getEntityName()
    {
        return 'follower';
    }
}
