<?php
namespace crawler\factories;

use crawler\Proxy;
use crawler\Config;
use crawler\db\MySql;
use crawler\exceptions\ConfigException;

class Factory
{
    # СЕРВИСЫ

    protected $namespace;
    /**
     * @var \crawler\Config $config
     */
    protected $config;
    /**
     * @var \crawler\db\MySql $db
     */
    private $db;
    /**
     * @var Model $domainModel
     */
    private $domainModel;
    /**
     * @var Proxy $proxy
     */
    private $proxy;
    /**
     * @var \crawler\adapters\InstagramAdapter $instagram
     */
    private $instagram;
    /**
     * @var Parser $parser парсер, извлекающий данные из инстаграма
     */
    private $parser;

    /**
     * @param \crawler\Config $config
     * @return void
     */
    public function __construct($namespace = '')
    {
        $this->config = new Config;
        $this->namespace = $namespace;
    }

    /**
     * @return \crawler\db\MySql
     */
    public function getDb()
    {
        if (is_null($this->db)) {
            $this->db = $this->createDb();
        }
        return $this->db;
    }

    /**
     * Создаем экземпляр DBAL
     * @return \crawler\db\MySql
     */
    public function createDb()
    {
        return new MySql($this->config->getAppConf('database'));
    }

    /**
     * @return \crawler\Config
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @return Model
     */
    public function getDomainModel()
    {
        if (is_null($this->domainModel)) {
            $this->domainModel = $this->createDomainModel();
        }
        return $this->domainModel;
    }

    /**
     * Создаем экземпляр Model
     * @return Model
     */
    public function createDomainModel()
    {
        $className = "\\{$this->namespace}\\models\\" . ucfirst($this->getEntityName()) . 'Model';
        return new $className($this->getDb(), $this->getInstagram());
    }

    /**
     * Создает экземпляр парсера
     * 
     * @param int $num номер парсера
     * @return Parser
     */
    public function getParser($num = 0)
    {
        if (is_null($this->parser)) {
            // Создаем экземпляр парсера
            $this->parser = $this->createParser($num);
        }
        return $this->parser;
    }

    /**
     * Создает экземпляр парсера
     * 
     * @param int $num номер парсера
     * @return Parser
     */
    public function createParser($num = 0)
    {
        $className = "\\{$this->namespace}\\parsers\\" . ucfirst($this->getEntityName()) . 'Parser';
        $parser = new $className($this);
        $parser->setNum($num);
        $parser->setMaxCachedModels($this->config->getAppConf('max_cached_models'));
        return $parser;
    }

    /**
     * @return \crawler\Proxy
     */
    public function getProxy()
    {
        if (is_null($this->proxy)) {
            $this->proxy = new Proxy($this->getDb());
            $tempDir = $this->config->getTempDir();
            $this->proxy
                ->setTableName($this->config->getAppConf('proxyTableName'))
                ->renewList("$tempDir/proxy-initial.txt")
                ->renewList("$tempDir/proxy-initial-ipv6.txt");
        }   
        return $this->proxy;
    }

    /**
     * @return mixed
     */
    public function getInstagram()
    {
        if (is_null($this->instagram)) {
            $this->instagram = $this->createInstagram();
        }
        return $this->instagram;
    }

    /**
     * Подключает библиотеку парсера
     */
    public function createInstagram()
    {
        $entityName = $this->getEntityName();
        $config = $this->config->getAppConf('instagram_parsers');
        if (!isset($config[$entityName])) {
            throw new ConfigException("Не задана библиотека парсера для $entityName.");
        }
        $config = $config[$entityName];
        $className = is_array($config) ? $config['class_name'] : $config;
        $className = "\\{$this->namespace}\\adapters\\$className";
        return new $className($this->config);
    }
}
