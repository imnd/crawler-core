<?php
namespace crawler\factories;

class UserStoryFactory extends Factory
{
    /**
     * Сущность, с которой работает парсер
     * 
     * @return string
     */
    public function getEntityName()
    {
        return 'userStory';
    }
}
