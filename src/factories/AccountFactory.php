<?php
namespace crawler\factories;

class AccountFactory extends Factory
{
    /**
     * Сущность, с которой работает парсер
     * 
     * @return string
     */
    public function getEntityName()
    {
        return 'account';
    }
}
