<?php
namespace crawler\adapters;

abstract class InstagramAdapter
{
    /**
     * @var mixed $instagram
     */
    protected $instagram;

    /**
     * Извлечение акка по его ID
     * 
     * @param integer $accId
     * @return array
     */
    abstract public function getAccountById($id);

    /**
     * Извлечение акка по его логину
     * 
     * @param string $username
     * @return array
     */
    abstract public function getAccountByName($username);

    /**
     * Конвертируем аккаунт в массив для сохранения в БД
     * 
     * @param mixed $account
     * @return array
     */
    abstract public function accountToArray($account);

    /**
     * Извлекаем посты из Инстаграма
     * 
     * @param integer $accId
     * @param integer $mediasCount
     * @return array
     */
    abstract public function getPosts($accId, $mediasCount=null);

    /**
     * Конвертируем пост в массив для сохранения в БД
     * 
     * @param mixed $post
     * @return array
     */
    abstract public function postToArray($post);

    /**
     * Конвертируем посты в массивы для сохранения в БД
     * 
     * @param array $posts
     * @return array
     */
    protected function postsToArray(array $posts)
    {
        $adapter = $this;
        return array_map(function($post) use ($adapter) {
            return $adapter->postToArray($post);
        }, $posts);
    }

    /**
     * Извлечение фоловеров по ID акка
     * 
     * @param integer $accId
     * @param integer $mediasCount
     * @return array
     */
    abstract public function getFollowers($accId, $mediasCount=null);

    /**
     * Конвертируем фолловера в массив для сохранения в БД
     * 
     * @param mixed $follower
     * @return array
     */
    abstract public function followerToArray($follower);

    /**
     * Конвертируем фолловеров в массивы для сохранения в БД
     * 
     * @param array $followers
     * @return array
     */
    protected function followersToArray(array $followers)
    {
        $adapter = $this;
        return array_map(function($follower) use ($adapter) {
            return $adapter->followerToArray($follower);
        }, $followers);
    }
}
