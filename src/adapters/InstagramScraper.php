<?php
namespace crawler\adapters;

use crawler\models\Model;

class InstagramScraper extends InstagramAdapter
{
    public function __construct()
    {
        $this->instagram = new \InstagramScraper\Instagram;
    }

    /**
     * @inheritdoc
     */
    public function getAccountById($id)
    {
        if ($account = $this->instagram->getAccountById($id)) {
            return $this->accountToArray($account);
        }
    }

    /**
     * @inheritdoc
     */
    public function getAccountByName($username)
    {
        if ($account = $this->instagram->getAccount($username)) {
            return $this->accountToArray($account);
        }
    }

    /**
     * @inheritdoc
     */
    public function accountToArray($account)
    {
        return [
            'account_id' => $account->getId(),
            'username' => $account->getUsername(),
            'full_name' => $account->getFullName(),
            'biography' => $account->getBiography(),
            'external_link' => $account->getExternalUrl(),
            'avatar_url' => $account->getProfilePicUrl(),
            'media_count' => $account->getMediaCount(),
            'followers_count' => $account->getFollowedByCount(),
            'follows_count' => $account->getFollowsCount(),
            'is_private' => $account->isPrivate(),
            'is_verified' => $account->isVerified(),
            'status' => Model::STATUS_READY
        ];
    }

    /**
     * @inheritdoc
     */
    public function getPosts($accId, $mediasCount=null)
    {
        $posts = $this->instagram->getMediasByUserId($accId, $mediasCount);
        return $this->postsToArray($posts);
    }

    /**
     * Извлекаем посты из Инстаграма
     * 
     * @param integer $accId
     * @return array
     */
    public function getPostsByUserId($accId)
    {
        return $this->instagram->getMediasByUserId($accId);
    }

    /**
     * Извлекаем пост из Инстаграма
     * 
     * @param integer $mediaId
     * @return array
     */
    public function getPostById($mediaId)
    {
        return $this->instagram->getMediaById($mediaId);
    }

    /**
     * @inheritdoc
     */
    public function postToArray($post)
    {
        $caption = $post->getCaption();
        $captionArr = explode(' ', $caption);
        $tags = array();
        foreach ($captionArr as $word) {
            if (strpos($word, '#')===0) {
                $tags[] = str_replace('#', '', trim($word));
            }
        }
        $result = [
            'account_id' => $post->getOwnerId(),
            'post_id' => $post->getId(),
            'short_code' => $post->getShortCode(),
            'caption' => $caption,
            'comments_count' => $post->getCommentsCount(),
            'likes_count' => $post->getLikesCount(),
            'link' => $post->getLink(),
            'hr_image' => $post->getImageHighResolutionUrl(),
            'thumb_image' => $post->getImageThumbnailUrl(),
            'media_type' => $post->getType(),
            'location' => $post->getLocationId(),
            'tags' => json_encode($tags),
            'created_at' => date('Y-m-d H:i:s', $post->getCreatedTime()),
        ];
        return array_intersect_key($result, array_flip($this->tableFields));
    }

    public function getFollowers($accId, $mediasCount=null)
    {
        $followers = $this->instagram->getFollowers($accId, $mediasCount);
        return $this->followersToArray($followers);
    }

    /**
     * @inheritdoc
     */
    public function followerToArray($follower)
    {
        return [
            'account_id' => $model->getId(),
            'username' => $model->getUsername(),
            'caption' => $model->getCaption(),
            'posts_count' => $model->getMediaCount(),
            'followers_count' => $model->getFollowedByCount(),
            'avatar_url' => $model->getProfilePicUrl(),
            //'selection_id' => $model->(),
        ];
    }
}
