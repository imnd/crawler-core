<?php
namespace crawler\adapters;

use crawler\Logger;

class InstagramAPI extends InstagramAdapter
{
    private $config;
    private $debug;

    /**
     * @param crawler/Config $config
     */
    public function __construct($config)
    {
        $this->debug = $config->getAppConf('debug');
        $this->config = $config->getAppConf(['instagram_parsers', 'account']);
        $this->instagram = new \InstagramAPI\Instagram($this->debug, $this->config['truncated_debug']);
        $this->login();
    }

    /**
     * @param string $username
     * @param string $password
     * @return boolean
     */
    public function login()
    {
        if ($this->debug) {
            // берем из конфига.
            $username = $this->config['credentials']['username'];
            $password = $this->config['credentials']['password'];
        } else {
            // берем из БД.
            // они должны быть привязаны к определенному прокси
        }
        try {
            $this->instagram->login($username, $password);
        } catch (\Exception $e) {
            echo "Can`t login with username: $username, password: $password. {$e->getMessage()}\n";
            exit(0);
        }
    }

    /**
     * @inheritdoc
     */
    public function getAccountById($id)
    {
        $account = $this->instagram->people->getInfoById($id)->getUser();
        Logger::log(print_r(get_class_methods($account), true));
        Logger::log(print_r($this->accountToArray($account), true));die;
        return $this->accountToArray($account);
    }

    /**
     * @inheritdoc
     */
    public function getAccountByName($username)
    {
        $account = $this->instagram->people->getInfoByName($username);
        return $this->accountToArray($account);
    }

    /**
     * @inheritdoc
     */
    public function accountToArray($account)
    {
        return [
            'account_id' => $account->getPk(),
            'username' => $account->getUsername(),
            'full_name' => $account->getFullName(),
            'biography' => $account->getBiography(),
            'external_link' => $account->getExternalUrl(),
            'avatar_url' => $account->getProfilePicUrl(),
            'media_count' => $account->getMediaCount(),
            'followers_count' => $account->getFollowingCount(),
            'follows_count' => $account->getFollowerCount(),
            'is_private' => $account->getIsPrivate(),
            'is_verified' => $account->getIsVerified(),
            'status' => self::STATUS_READY
        ];
    }

    /**
     * @inheritdoc
     */
    public function getPosts($accId, $mediasCount=null)
    {
        $maxId = null;
        $posts = array();
        do {
            /** @var \InstagramAPI\Response\UserFeedResponse $response */
            $response = $this->instagram->timeline->getUserFeed($Pk, $maxId);
            $posts = array_merge($posts, $response->getItems());
            $maxId = $response->getNextMaxId();
            // Sleep for 5 seconds before requesting the next page. This is just an
            // example of an okay sleep time. It is very important that your scripts
            // always pause between requests that may run very rapidly, otherwise
            // Instagram will throttle you temporarily for abusing their API!
            sleep(5);
        } while ($maxId !== null);

        return $this->postsToArray($posts);
    }

    /**
     * @inheritdoc
     */
    public function postToArray($post)
    {
        $caption = $post->getCaption();
        $captionArr = explode(' ', $caption);
        $tags = array();
        foreach ($captionArr as $word) {
            if (strpos($word, '#')===0) {
                $tags[] = str_replace('#', '', trim($word));
            }
        }
        $result = [
            'account_id' => $post->getMedia()->getUser()->getPk(), // ?
            'post_id' => $post->getPk(),
//            'short_code' => $post->getShortCode(),
            'caption' => $caption,
            'comments_count' => count($post->getComments()),
//            'likes_count' => $post->getLikesCount(),
            'link' => $post->getLink(),
//            'hr_image' => $post->getImageHighResolutionUrl(),
//            'thumb_image' => $post->getImageThumbnailUrl(),
//            'media_type' => $post->getType(),
            'location' => $post->getLocation(),
            'tags' => json_encode($tags),
            'created_at' => date('Y-m-d H:i:s', $post->getCreatedTime()),
        ];
    }

    public function getFollowers($accId, $mediasCount=null)
    {
        $followers = $this->instagram->people->getFollowers($accId);
        return $this->followersToArray($followers);
    }

    public function followerToArray($follower)
    {
        return [
            //'account_id' => $follower->getId(),
            //'username' => $follower->getUsername(),
            //'caption' => $follower->getCaption(),
            //'posts_count' => $follower->getMediaCount(),
            //'followers_count' => $follower->getFollowedByCount(),
            //'avatar_url' => $follower->getProfilePicUrl(),
            //'selection_id' => $follower->(),
        ];
    }
}
