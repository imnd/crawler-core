<?php
namespace crawler\parsers;

use crawler\Logger;

class AccountParser extends Parser
{
    /** @const Размер таблиц акков */
    const MAX_PARSE_IDS = 1000;

    /**
     * @inheritdoc
     */
    protected function isStoreTableFits()
    {
        if (is_null($this->currentAccountId)) {
            $this->currentAccountId = $this->domainModel->getLastId($this->storeTableName);
        }
        // устанавливаем очередной ID для парсинга
        // и проверяем принадлежность диапазону
        return ++$this->currentAccountId <= $this->getTableNum($this->storeTableName) * self::MAX_PARSE_IDS;
    }

    /**
     * @inheritdoc
     */
    protected function getInstagramModels()
    {
        //if ($model = $this->instagram->getAccountById($this->currentAccountId)) {
        if ($model = $this->getInstagramModelsByLogin()) {
            return array($model);
        }
    }

    /**
     * Извлекаем модель
     * @return \InstagramScraper\Model\Account
     */
    public function getItem($login)
    {
        while (null===$model = $this->getModelByLogin($login)) {
            $this->setNewProxy();
            continue;
        }
        return $model;
    }

    /**
     * Извлекаем запись из Инстаграма по его username
     * 
     * @return \InstagramScraper\Model\Account
     */
    protected function getInstagramModelsByLogin()
    {
        if ($posts = $this->instagram->getPostsByUserId($this->currentAccountId)) {
            return $posts[0]->getOwner();
        }
    }

    /**
     * Извлекаем запись из Инстаграма
     * 
     * @return [\InstagramScraper\Model\Account]
     */
    protected function getModelByLogin($login)
    {
        try {
            return $this->instagram->getAccountByName($login);
        } catch (Throwable $err) {
            return null;
        } catch (\Exception $err) {
            return null;
        }
    }
}
