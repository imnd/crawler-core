<?php
namespace crawler\parsers;

use crawler\factories\Factory;
use crawler\Logger;

abstract class Parser
{
    const SLEEP_TIME_BETWEEN_REQUESTS = 5;
    const SLEEP_TIME_WHEN_FORBIDDEN = 60 * 3;
    const SLEEP_TIME_WHEN_NO_FREE_PROXY = 60 * 60 * 3;
   
    /**
     * @var \InstagramScraper\Instagram $instagram
     */
    protected $instagram;
    /**
     * Название таблицы, в которую складываются извлеченные данные
     * @var string $storeTableName
     */
    protected $storeTableName;
    /**
     * Текущий ID акка
     * @var string $currentAccountId
     */
    protected $currentAccountId;
    /**
     * Накапливаем данные в памяти, что бы сбрасывать периодически в БД
     * @var array $storedModels
     */
    protected $cachedModels = [];
    /**
     * Количество записей, которые зранятся в памяти перед сливом в БД
     * @var int $maxCachedModels
     */
    protected $maxCachedModels = 1;
    /**
     * Номер парсера
     * @var int $num
     */
    protected $num = 0;

    # СЕРВИСЫ

    /**
     * @var \crawler\Factory $factory
     */
    protected $factory;
    /**
     * @var \crawler\Model $domainModel
     */
    protected $domainModel;
    /**
     * @var \crawler\Proxy $proxy
     */
    protected $proxy;

    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
        //$this->db = $this->factory->getDb();
        $this->instagram = $this->factory->getInstagram();
        $this->domainModel = $this->factory->getDomainModel();
        $this->proxy = $this->factory->getProxy();
    }

    public function parse()
    {
        while (true) {
            // устанавливаем текущую таблицу для сохранения акков
            $this->chooseStoreTable();
            // извлекаем модели до тех пор пока не подохнут прокси
            do {
                if (!$this->checkProxy()) {
                    // убиваем скрипт т.к. нет свободных прокси
                    $this->sleep(self::SLEEP_TIME_WHEN_NO_FREE_PROXY);
                }
                $models = $this->getModels();
            } while (null===$models);
            // кешируем модели
            $this->cacheModels($models);
            // если кеш полон
            if (count($this->cachedModels) >= $this->maxCachedModels) {
                // скидываем в БД
                $this->flushModels();
            }
            // спим
            sleep(self::SLEEP_TIME_BETWEEN_REQUESTS);
        }
    }

    /**
     * Проверяет жив ли прокси, отрубает если надо и подключается к новому
     * @return void
     */
    protected function checkProxy()
    {
        $this->proxy->free();
        do {
            if (!$this->proxy->change()) {
                return false;
            }
            if ($errorsOverflow = $this->proxy->errorsOverflow()) {
                $this->proxy
                    ->setLastUseTime()
                    ->setAsDead();
            }
        } while ($errorsOverflow);
        
        return true;
    }

    /**
     * Выбирает и устанавливает текущую таблицу для сохранения акков
     * @return void
     */
    protected function chooseStoreTable()
    {
        while ($this->storeTableName = $this->getStoreTableName()) {
            //Logger::log("Try table {$this->storeTableName}.");
            if ($this->isStoreTableFits()) {
                //Logger::log("Choose table {$this->storeTableName}.");
                $this->lockStoreTable();
                $this->domainModel->setTableName($this->storeTableName);
                return;
            }
            Logger::log("Table {$this->storeTableName} is parsed.");
            // если в таблицу писать нельзя, то у неё статус full и она должна быть locked
            $this->dischargeTable();
            $this->currentAccountId = null;
        }
        Logger::log("There is no parse table. The last table was: {$this->storeTableName}.");
        Logger::err("All tables are locked. 1 process == 1 table. If you shure that there is no runned processes enter \"php parser.php unlock-tables {$this->factory->getEntityName()}\" from root dir, then \"php parser.php parse {$this->factory->getEntityName()}\"");
        // скидываем в БД
        $this->flushModels();
        // убиваем скрипт т.к. все id спарсли
        $this->_exit();
    }

    protected function dischargeTable()
    {
        $this->domainModel->dischargeTable($this->storeTableName);
        $this->storeTableName = null;
    }

    protected function getStoreTableName()
    {
        if (!is_null($this->storeTableName)) {
            return $this->storeTableName;
        }
        return $this->domainModel->getFreeTable();
    }

    /**
     * @return void
     */
    protected function lockStoreTable()
    {
        $this->domainModel->setStoreTableStatus($this->storeTableName, 'locked');
    }

    protected function getTableNum($storeTableName)
    {
        return filter_var($storeTableName, FILTER_SANITIZE_NUMBER_INT);
    }

    /**
     * Извлекаем записи из Инстаграма
     * 
     * @return [\InstagramScraper\Model\AbstractModel]
     */
    protected function getModels()
    {
        $this->proxy->change();
        try {
            $models = $this->getInstagramModels();

            if ($this->proxy->isClean()) {
                $this->proxy->setFirstUseTime();
            }
            $this->proxy->encreaseRequestsCounter();

            return $models;
        } catch (Throwable $err) {
            return $this->handleError($err);
        } catch (\Exception $err) {
            return $this->handleError($err);
        }
    }

    /**
     * @param mixed $err
     * @return void
     */
    protected function handleError($err)
    {
        $errCode = $err->getCode();
        $errMsg = $err->getMessage();
        switch ($errCode) {
            case 404:
                $this->logErr("Fail get {$this->factory->getEntityName()}, error #$errCode, message: $errMsg account id = {$this->currentAccountId}");
                return [];
            case 429:
                Logger::err("Fail get {$this->factory->getEntityName()}, error #$errCode, message: $errMsg", 'proxy');
                $this->sleep(self::SLEEP_TIME_WHEN_FORBIDDEN);
                return [];
            case 500:
                Logger::err("Fail get {$this->factory->getEntityName()}, error #$errCode, message: $errMsg", 'proxy');
                $this->proxy->encreaseErrCounter();
                return null;
            default:
                Logger::err("Unknown instagram error #$errCode, message: $errMsg", 'proxy');
                $this->proxy->encreaseErrCounter();
                return null;
        }
    }

    /**
     * Сохраняем модели в память
     * 
     * @param array $models
     * @return void
     */
    protected function cacheModels(array $models)
    {
        $this->cachedModels = array_merge($this->cachedModels, $models);
    }

    /**
     * Сохраняем модель в память
     * 
     * @param array $models
     * @return void
     */
    protected function cacheModel(array $model)
    {
        $this->cachedModels[] = $model;
    }

    /**
     * Сохраняем модели в БД
     * 
     * @return void
     */
    public function flushModels()
    {
        if (!empty($this->cachedModels)) {
            $this->domainModel->saveModels($this->cachedModels);
            $this->cachedModels = [];
        }
    }

    protected function sleep($time)
    {
        Logger::log("Go to sleep for $time seconds");
        sleep($time);
        Logger::log('Awake and continue parse');
    }

    /**
     * @param string $msg
     * @return void
     */
    protected function logErr($msg)
    {
        Logger::err($msg, $this->storeTableName ? $this->storeTableName : 'error');
    }

    private function _exit()
    {
        Logger::log('Kill process ID ' . getmypid());
        $this->proxy->free();
        $this->domainModel->setStoreTableStatus($this->storeTableName, 'free');
        $this->domainModel->closeConnection();

        exit;
    }

    # SETTERS AND GETTERS

    /**
     * @return \crawler\Model
     */
    public function getDomainModel()
    {
        return $this->domainModel;
    }

    /**
     * Устанавливает текущую таблицу для сохранения акков
     * @return \crawler\Parser
     */
    public function setStoreTable($tableName)
    {
        $this->storeTableName = $tableName;
        return $this;
    }

    /**
     * @param int $num 
     * @return \crawler\Parser
     */
    public function setMaxCachedModels($num)
    {
        $this->maxCachedModels = $num;
        return $this;
    }

    /**
     * @param int $num Номер парсера
     * @return \crawler\Parser
     */
    public function setNum($num)
    {
        $this->num = $num;
        return $this;
    }
}
