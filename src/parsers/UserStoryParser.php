<?php
namespace crawler\parsers;

use crawler\factories\UserStoryFactory;

class UserStoryParser extends Parser
{
    use \crawler\traits\AccountChildrenParser;

    /** @const Сколько постов выдергиваем */
    const MEDIAS_COUNT = 1000;

    /**
     * @var \crawler\account\Model $accDomainModel
     */
    protected $accDomainModel;

    public function __construct(Factory $factory)
    {
        parent::__construct($factory);

        $this->accDomainModel = (new \crawler\account\Factory($factory->getConfig()))->createDomainModel();
    }

    /**
     * Извлекаем посты из Инстаграма
     */
    protected function getInstagramModels()
    {
        return $this->instagram->getPosts($this->currentAccountId, static::MEDIAS_COUNT);
    }
}
