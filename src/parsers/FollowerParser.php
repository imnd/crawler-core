<?php
namespace crawler\parsers;

class FollowerParser extends Parser
{
    use \crawler\traits\AccountChildrenParser;

    /** @const Сколько постов выдергиваем */
    const MEDIAS_COUNT = 1000;

    /**
     * @var \crawler\account\Model $accDomainModel
     */
    protected $accDomainModel;

    public function __construct(Factory $factory)
    {
        parent::__construct($factory);
        $this->accDomainModel = (new \crawler\account\Factory($factory->getConfig()))->createDomainModel();
    }

    /**
     * Извлекаем фоловеров
     * @return \InstagramScraper\Model\Followers
     */
    public function getFollowers($accId)
    {
        $this->currentAccountId = $accId;
        while (null===$followers = $this->getModels()) {
            $this->setNewProxy();
            continue;
        }
        return $followers;
    }

    /**
     * Извлекаем фоловеров из Инстаграма
     */
    protected function getInstagramModels()
    {
        return $this->instagram->getFollowers($this->currentAccountId, self::MEDIAS_COUNT);
    }
}
